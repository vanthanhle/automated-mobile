package commons;

public class Constants {
    public static final int SHORT_TIMEOUT = 5;
    public static final int LONG_TIMEOUT = 30;
}
